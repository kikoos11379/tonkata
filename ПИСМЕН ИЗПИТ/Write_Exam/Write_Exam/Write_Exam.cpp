﻿#include <iostream>
#include<cstring>
#include "Cars.h"
using namespace std;

//ЗАДАЧА 1 - КОЛИТЕ 
/*Да се реализира клас, представящ превозно средство със следните характеристики: регистрационен
номер (низ с фиксирана дължина до 8 символа), марка (низ с произволна дължина) и разход на гориво
за 100 км (реално число).
Да се дефинират подходящи конструктори, селектори и мутатори.
Да се дефинира масив от превозни средства, което представлява гараж за автомобили под наем. Да се
дефинира функция, която по зададен регистрационен номер проверява дали превозното средство се
намира в гаража.
Да се дефинира функция, която по зададена марка връща автомобила с най-малък среден разход на
гориво.*/
#pragma warning(disable : 4996)

char* copy_char(const char* old)//за копи конструктора,ако имаме char или string,трябва да направим такава функция
{
	if (!old)
	{
		old = "";
	}
	char* new_ = new char[strlen(old) + 1];
	strcpy(new_, old);
	return new_;
}

cars::cars()//разписваме конструктор по подразбиране
{
	reg_num = nullptr;
	label = nullptr;
	fuel = 0;
}
cars::cars(const char* reg_num1, const char* label1, const int fuel1)//разписваме конструктор с параметри
{
	set_reg_num(reg_num1);
	set_label(label1);
	set_fuel(fuel1);
}
cars::cars(const cars& other)//разписваме копи конструктор
{
	reg_num = copy_char(other.reg_num);
	label = copy_char(other.label);
	fuel = other.fuel;
}
cars& cars::operator=(const cars& other)//разписваме оператор =
{
	if (this != &other)
	{
		reg_num = other.get_reg_num();
		label = other.get_label();
		fuel = other.get_fuel();
	}
	return *this;
}
cars::~cars()//разписваме деконструктора,освобождава динамична памет
{
	//delete[]reg_num;
	delete[]label;
}
//разписваме getters и setters
char* cars::get_reg_num()const
{
	return reg_num;
}
char* cars::get_label()const
{
	return label;
}
int cars::get_fuel()const
{
	return fuel;
}

void cars::set_reg_num(const char* new_reg_num)
{
	reg_num = copy_char(new_reg_num);
}
void cars::set_label(const char* new_label)
{
	label = copy_char(new_label);
}
void cars::set_fuel(const int new_fuel)
{
	fuel = new_fuel;
}


void pr(const char* arr)//принтира масива от char-ове
{
	for (int i = 0; i < strlen(arr); i++)
	{
		cout << arr[i];
	}
	cout << endl;
}

bool find_car(const char* source, cars arr[], int size)//проверяваме дали колата е в гаража(гаража е масива от коли)
{
	for (int i = 0; i < size; i++)
	{
		if (!strcmp(arr[i].get_reg_num(), source))
		{
			return true;
		}
	}
	return false;
}

char* strTOchar(string str)//превръща от string в char
{
	char* res = new char[str.length() + 1];
	strcpy(res, str.c_str());
	return res;
}

int main()
{
	cars aer[4];//масив от 4 коли
	aer[0].set_reg_num("194151651");
	aer[0].set_label("opel");
	aer[0].set_fuel(8);

	aer[1].set_reg_num("18181456");
	aer[1].set_label("lada");
	aer[1].set_fuel(5);

	aer[2].set_reg_num("654165156");
	aer[2].set_label("opel");
	aer[2].set_fuel(6);

	aer[3].set_reg_num("5161561");
	aer[3].set_label("bmw");
	aer[3].set_fuel(10);

	string input;
	getline(cin, input);//чете input
	char* find = strTOchar(input);//from string(input) to char*(find)
	cout << find_car(find, aer, 4);//извеждаме на конзолата функцията find_car,която приема масива от коли и размера(4)


	return 0;
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
/*Задача 2
Дадено е цяло неотрицателно число.Да се използва структурата от данни стек, за да се преобразува
числото в двоична бройна система.Резултатът да се изведе на екрана.*/

#include <iostream>
#include "my_stack.h"

using namespace std;

void print_st(my_stack<int> st)
{
	while (!st.isEmpty())
	{
		cout << st.peek();
		st.pop();
	}
}

int main()
{
	int num;
	cin >> num;
	int rem;
	my_stack<int> binary;

	while (num > 0)
	{
		rem = num % 2;
		binary.push(rem);
		num = num / 2;
	}

	print_st(binary);

	return 0;
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//ЗАДАЧА 3 - ОПАШКА
/*
Дадени са двe опашки от цели числа, подредени в нарастващ ред от началото към края.Да се дефинира
функция, която построява нова опашка, състояща се от всички елементи на дадените две, подредени в
нарастващ ред от началото към края.
Пример: 1, 3, 5 1, 2, 6 -> 1, 1, 2, 3, 5, 6*/

#include <iostream>
#include <queue>
#include <stack>
#include <string>



using namespace std;

template<typename T>
void prQ(queue<T> q)
{
	while (!q.empty())
	{
		cout << q.front() << " ";//front-дава достъп до следващия елемент
		q.pop();// премахва следващ елемент
	}
}

void fill_queue(queue<char>& q)
{
	cout << "-> ";
	string inp;
	getline(cin, inp);
	int i = 0;
	while (inp[i])
	{
		char b = inp[i];
		q.push(b);
		i++;
	}
}


queue<int> big(queue<int> q1, queue<int> q2)//тук сортира по големина
{
	queue<int> res;
	while (!q1.empty() && !q2.empty())
	{
		if (q1.front() < q2.front())//сравнява
		{
			res.push(q1.front());//първо по-малкия
			res.push(q2.front());//после по-големия 
		}
		else
		{
			res.push(q2.front());//първо по-малкия
			res.push(q1.front());//после по-големия
		}

		q1.pop();//трие следващия елемент
		q2.pop();//трие следващия елемент
	}

	return res;//връша новата сортирана по възходящ ред опашка опашка
}


int main()
{
	queue<int> lis;
	fill_queue(lis);
	cout << big(lis);

	return 0;
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
/*Задача 4
В опашка е записан без грешка израз от вида:
<израз> ::= <цифра> | f(<израз>) | g(<израз>),
където f(x) = х + 1, f(9) = 0 и g(x) = x-1, а g(0) = 9.
Да се дефинира функция, която намира стойността на израза чрез стек*/
char cal(queue<char> q)
{
	stack<char> st;
	char curr, ch, op, a;

	while (!q.empty())
	{
		curr = q.front();

		if (curr == 'f' || curr == 'g' || (curr >= '0' && curr <= '9'))
		{
			st.push(curr);
		}
		else if (curr == ')')
		{
			a = st.top();
			st.pop();
			op = st.top();
			st.pop();

			switch (op)
			{
			case 'f':
				if (a >= '0' && a <= '8')
				{
					st.push(a + 1);
				}
				else
				{
					st.push('0');
				}
				break;

			case 'g':
				if (a >= '1' && a <= '9')
				{
					st.push(a - 1);
				}
				else
				{
					st.push('9');
				}
				break;
			}
		}

		q.pop();
	}

	ch = st.top();
	return ch;
}
//-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=--=-=-=
/*Задача 5
Даден е едносвързан списък от цифри, представен с указател към началото на списъка. Размерът на
списъка не надвишава 9. Да се напише функция, която намира най-голямото число, което може да се
прочете от цифрите в списъка.
*/
#include <iostream>
#include <stack>
#include <string>
#include <forward_list>//за едносвързан списък

using namespace std;

/*example:
	 Declaring forward list
	 forward_list<int> flist1; */

template<typename T>
void fill_list(forward_list<int>& l)//попълва листа
{
	int i = 0;
	int data;
	while (i < 9)
	{
		cin >> data;
		l.push_front(data);//вкарва следващ елемент
		i++;
	}
}


int biggest(forward_list<int> l)
{
	forward_list<int>::iterator i = l.begin();
	int res = *i;
	while (i != l.end())
	{
		if (*i > res)
		{
			res = *i;
		}

		i++;
	}

	return res;
}
int main()
{
	forward_list<int> lis;
	fill_list(lis);
	cout << biggest(lis);

	return 0;
}
//=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=--=--=-=-=
/*Задача 6
Дадено е двоично дърво, чиито елементи са реални числа. Да се напише функция,
която намира средноаритметичното на елементите, записани в дървото.
*/
struct Node
{
	Node* left;//лява част
	Node* right;//дясна част
	int data;//текущ елемент
};

Node* newNode(int value)//нов възел(дърво)
{
	Node* newNode1 = new Node;//нов възел
	newNode1->data = value;//присвояваме на текущия елемент нова стойност
	newNode1->left = nullptr;//ляво да е празно
	newNode1->right = nullptr;//дясно да е празно

	return newNode1;//и го връшане
}

void inOrder(Node* root)//ляво,корен,дясно
{
	if (root == nullptr)
	{
		return;
	}
	else
	{
		inOrder(root->left);
		cout << root->data << " ";
		inOrder(root->right);
	}
}

void insert(Node*& root, int data)
{
	if (root == nullptr)
	{
		root = newNode(data);//имаме си само един текущ елемент
		return;
	}
	if (data <= root->data)//ако корена е по-голям от даден елемент
	{
		insert(root->left, data);//елемента отива в лявата част от корена
	}
	else
	{
		insert(root->right, data);//елемента отива в дясната част от корена
	}
}

int getcountNodes(Node* root)//намира броя възли
{
	if (root == nullptr) return 0;//ако корена е празен,връщаме 0,защото няма елементи
	return (getcountNodes(root->left) + getcountNodes(root->right) + 1);//или ляво+дясно + 1(за корена)
}

int sum_of_nodes(Node* root)//сумата
{
	if (root == nullptr)
	{
		return 0;//ако корена е празен,връща 0,защото няма елементи
	}
	else
	{
		return root->data + sum_of_nodes(root->left) + sum_of_nodes(root->right);//или корен + ляво + дясно
	}

}

double average(Node* root)//средно аритметично
{
	double res = (double)sum_of_nodes(root) / (double)getcountNodes(root);//сумата я делим на броя възли
	return res;
}


int main()
{
	int sum = 0;

	Node* root = nullptr;
	while (true)
	{
		int i;
		cin >> i;
		if (i == 0) break;
		insert(root, i);
	}
	inOrder(root);
	cout << endl;

	cout << average(root);
	

	return 0;
}

//-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=--=-=-=
/*Задача 7
Нека е дадено двоично наредено дърво с числа по върховете. Да се провери дали релацията между
елементите на дървото е строга, т.е. елементите вдясно от корена са строго по-големи от корена, а
елементите вляво от корена - строго по-малки от него.*/
struct Node
{
	Node* left;//лява част
	Node* right;//дясна част
	int data;//текущ елемент
};

Node* newNode(int value)//нов възел(дърво)
{
	Node* newNode1 = new Node;//нов възел
	newNode1->data = value;//присвояваме на текущия елемент нова стойност
	newNode1->left = nullptr;//ляво да е празно
	newNode1->right = nullptr;//дясно да е празно

	return newNode1;//и го връшане
}

void inOrder(Node* root)//ляво,корен,дясно
{
	if (root == nullptr)
	{
		return;
	}
	else
	{
		inOrder(root->left);
		cout << root->data << " ";
		inOrder(root->right);
	}
}

void insert(Node*& root, int data)
{
	if (root == nullptr)
	{
		root = newNode(data);//имаме си само един текущ елемент
		return;
	}
	if (data <= root->data)//ако корена е по-голям от даден елемент
	{
		insert(root->left, data);//елемента отива в лявата част от корена
	}
	else
	{
		insert(root->right, data);//елемента отива в дясната част от корена
	}
}
bool isEqual(Node* root, int x)
{
	if (root == nullptr)
	{
		return false;
	}
	return root->data == x
		|| isEqual(root->left, x)
		|| isEqual(root->right, x);
}
bool check(Node* root)
{
	if (root == nullptr)
	{
		return false;
	}
	return isEqual(root->left, root->data) || isEqual(root->right, root->data)
		|| check(root->right) || check(root->left);
}
int main()
{
	int sum = 0;

	Node* root = nullptr;
	while (true)
	{
		int i;
		cin >> i;
		if (i == 0) break;
		insert(root, i);
	}
	inOrder(root);
	cout << endl;

	
	cout << check(root);

	return 0;
}
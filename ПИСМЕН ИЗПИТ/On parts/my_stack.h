#pragma once

using namespace std;

template<class T>
class my_stack
{
public:
	my_stack();
	my_stack(const my_stack& other);
	my_stack& operator= (const my_stack& other);
	~my_stack();

	int size()const;
	bool isEmpty()const;
	void clear();
	void push(T data);
	T pop();
	T peek()const;

private:
	static int const INITIAL_CAPACITY = 10;
	T* arr;
	int capacity;
	int count;
	void expandCapacity();
	void deepCopy(const my_stack& other);
};

template<class T>
my_stack<T>::my_stack()
{
	capacity = INITIAL_CAPACITY;
	arr = new T[capacity];
	count = 0;
}

template<class T>
my_stack<T>::my_stack(const my_stack& other)
{
	deepCopy(other);
}

template<class T>
my_stack<T>& my_stack<T>::operator=(const my_stack<T>& other)
{
	if (&other != this)
	{
		delete[]arr;
		deepCopy(other);
	}
	return *this;
}

template<class T>
my_stack<T>::~my_stack()
{
	delete[]arr;
}

template<class T>
int my_stack<T>::size()const
{
	return count;
}

template<class T>
bool my_stack<T>::isEmpty()const
{
	return count == 0;
}

template<class T>
void my_stack<T>::clear()
{
	count = 0;
}

template<class T>
void my_stack<T>::push(T data)
{
	if (capacity == count)
	{
		expandCapacity();
	}
	arr[count++] = data;
}

template<class T>
T my_stack<T>::pop()
{
	if (isEmpty())
	{
		cerr << "Can't pop an empty stack";
		exit(1);
	}
	return arr[--count];
}

template<class T>
T my_stack<T>::peek()const
{
	if (isEmpty())
	{
		cerr << "Can't peek an empty stack";
		exit(1);
	}
	return arr[count - 1];
}



template<class T>
void my_stack<T>::expandCapacity()
{
	T* old = arr;
	capacity *= 2;
	arr = new T[capacity];
	for (int i = 0; i < count; i++)
	{
		arr[i] = old[i];
	}
	delete[]old;
}

template<class T>
void my_stack<T>::deepCopy(const my_stack& other)
{
	count = other.count;
	capacity = other.capacity;
	arr = new T[capacity];
	for (int i = 0; i < count; i++)
	{
		arr[i] = other.arr[i];
	}
}

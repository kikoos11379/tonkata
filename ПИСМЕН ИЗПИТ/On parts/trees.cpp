#include<iostream>

using namespace std;

struct Node
{
	Node* left;
	Node* right;
	int data;
};

Node* newNode(int value)
{
	Node* newNode1 = new Node;
	newNode1->data = value;
	newNode1->left = nullptr;
	newNode1->right = nullptr;

	return newNode1;
}

void inOrder(Node* root)
{
	if (root == nullptr)
	{
		return;
	}
	else
	{
		inOrder(root->left);
		cout << root->data << " ";
		inOrder(root->right);
	}
}

void insert(Node*& root, int data)
{
	if (root == nullptr)
	{
		root = newNode(data);
		return;
	}
	if (data <= root->data)
	{
		insert(root->left, data);
	}
	else
	{
		insert(root->right, data);
	}
}

int getcountNodes(Node* root)
{
	if (root == nullptr) return 0;
	return (getcountNodes(root->left) + getcountNodes(root->right) + 1);
}

int sum_of_nodes(Node* root)
{
	if (root == nullptr)
	{
		return 0;
	}
	else
	{
		return root->data + sum_of_nodes(root->left) + sum_of_nodes(root->right);
	}

}

double average(Node* root)
{
	double res = (double)sum_of_nodes(root) / (double)getcountNodes(root);
	return res;
}

bool isEqual(Node* root, int x)
{
	if (root == nullptr)
	{
		return false;
	}
	return root->data == x
		|| isEqual(root->left, x) 
		|| isEqual(root->right, x);
}

bool check(Node* root)
{
	if (root == nullptr)
	{
		return false;
	}
	return isEqual(root->left, root->data) || isEqual(root->right, root->data) 
		|| check(root->right) || check(root->left);
}


int main()
{
	int sum = 0;

	Node* root = nullptr;
	while (true)
	{
		int i;
		cin >> i;
		if (i == 0) break;
		insert(root, i);
	}	
	inOrder(root);
	cout << endl;

	//cout << average(root);
	cout << check(root);

	return 0;
}
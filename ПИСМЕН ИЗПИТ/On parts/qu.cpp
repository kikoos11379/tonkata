#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <forward_list>

using namespace std;

template<typename T>
void prQ(queue<T> q)
{
	while (!q.empty())
	{
		cout << q.front() << " ";
		q.pop();
	}
}

void fill_list(forward_list<int>& l)
{
	int i = 0;
	int data;
	while (i < 9)
	{
		cin >> data;
		l.push_front(data);
		i++;
	}
}

int biggest(forward_list<int> l)
{
	forward_list<int>::iterator i = l.begin();
	int res = *i;
	while (i != l.end())
	{
		if (*i > res)
		{
			res = *i;
		}

		i++;
	}

	return res;
}

void fill_queue(queue<char>& q)
{
	cout << "-> ";
	string inp;
	getline(cin, inp);
	int i = 0;
	while (inp[i])
	{
		char b = inp[i];
		q.push(b);
		i++;
	}
}

queue<int> big(queue<int> q1, queue<int> q2)
{
	queue<int> res;
	while (!q1.empty() && !q2.empty())
	{
		if (q1.front() < q2.front())
		{
			res.push(q1.front());
			res.push(q2.front());
		}
		else
		{
			res.push(q2.front());
			res.push(q1.front());
		}

		q1.pop();
		q2.pop();
	}

	return res;
}

char cal(queue<char> q)
{
	stack<char> st;
	char curr, ch, op, a;

	while (!q.empty())
	{	
		curr = q.front();
		
		if (curr == 'f' || curr == 'g' || (curr >= '0' && curr <= '9'))
		{
			st.push(curr);
		}
		else if (curr == ')')
		{
			a = st.top();
			st.pop();
			op = st.top();
			st.pop();

			switch (op)
			{
			case 'f':
				if (a >= '0' && a <= '8')
				{
					st.push(a + 1);
				}
				else
				{
					st.push('0');
				}
				break;

			case 'g':
				if (a >= '1' && a <= '9')
				{
					st.push(a - 1);
				}
				else
				{
					st.push('9');
				}
				break;
			}
		}

		q.pop();
	}

	ch = st.top();
	return ch;
}

int main()
{
	forward_list<int> lis;
	fill_list(lis);
	cout << biggest(lis);

	return 0;
}
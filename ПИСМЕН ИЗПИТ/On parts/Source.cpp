#include <iostream>
#include "my_stack.h"

using namespace std;

void print_st(my_stack<int> st)
{
	while (!st.isEmpty())
	{
		cout << st.peek();
		st.pop();
	}
}

int main()
{
	int num;
	cin >> num;
	int rem;
	my_stack<int> binary;

	while (num > 0)
	{
		rem = num % 2;
		binary.push(rem);
		num = num / 2;
	}

	print_st(binary);

	return 0;
}
#pragma once
class cars
{
public:
	cars();
	cars(const char* reg_num1, const char* label, const int fuel1);
	cars(const cars& other);
	cars& operator=(const cars& other);
	~cars();

	char* get_reg_num()const;
	char* get_label()const;
	int get_fuel()const;

	void set_reg_num(const char* new_reg_num);
	void set_label(const char* new_label);
	void set_fuel(const int new_fuel);

private:
	char* reg_num;
	char* label;
	int fuel;
};


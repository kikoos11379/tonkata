#include <iostream>
#include "cars.h"
#include <string>
#include <cstring>
#pragma warning(disable : 4996)

using namespace std;

void pr(const char* arr)
{
	for (int i = 0; i < strlen(arr); i++)
	{
		cout << arr[i];
	}
	cout << endl;
}

bool find_car(const char* source, cars arr[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (!strcmp(arr[i].get_reg_num(), source))
		{
			return true;
		}
	}
	return false;
}

char* strTOchar(string str)
{
	char* res = new char[str.length() + 1];
	strcpy(res, str.c_str());
	return res;
}

int main()
{
	cars aer[4];
	aer[0].set_reg_num("194151651");
	aer[0].set_label("opel");
	aer[0].set_fuel(8);

	aer[1].set_reg_num("18181456");
	aer[1].set_label("lada");
	aer[1].set_fuel(5);

	aer[2].set_reg_num("654165156");
	aer[2].set_label("opel");
	aer[2].set_fuel(6);

	aer[3].set_reg_num("5161561");
	aer[3].set_label("bmw");
	aer[3].set_fuel(10);

	string input;
	getline(cin, input);
	char* find = strTOchar(input);
	cout << find_car(find, aer, 4);


	return 0;
}
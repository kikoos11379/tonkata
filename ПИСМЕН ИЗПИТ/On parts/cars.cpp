#include "cars.h"
#include<cstring>

#pragma warning(disable : 4996)

char* copy_char(const char* old)
{
	if (!old)
	{
		old = "";
	}
	char* new_ = new char[strlen(old) + 1];
	strcpy(new_, old);
	return new_;
}

cars::cars()
{
	reg_num = nullptr;
	label =nullptr;
	fuel = 0;
}
cars::cars(const char* reg_num1, const char* label1, const int fuel1)
{
	set_reg_num(reg_num1);
	set_label(label1);
	set_fuel(fuel1);
}
cars::cars(const cars& other)
{
	reg_num = copy_char(other.reg_num);
	label = copy_char(other.label);
	fuel = other.fuel;
}
cars& cars::operator=(const cars& other)
{
	if (this != &other)
	{
		reg_num = other.get_reg_num();
		label = other.get_label();
		fuel = other.get_fuel();
	}
	return *this;
}
cars::~cars()
{
	//delete[]reg_num;
	delete[]label;
}

char* cars::get_reg_num()const
{
	return reg_num;
}
char* cars::get_label()const
{
	return label;
}
int cars::get_fuel()const
{
	return fuel;
}

void cars::set_reg_num(const char* new_reg_num)
{
	reg_num = copy_char(new_reg_num);
}
void cars::set_label(const char* new_label)
{
	label = copy_char(new_label);
}
void cars::set_fuel(const int new_fuel)
{
	fuel = new_fuel;
}
﻿#include <iostream>
#include <queue>

using namespace std;

struct Node
{
	Node* left;
	Node* right;
	int data;
};

// 1
Node* NEWNODE(int value)
{
	Node* newnode = new Node;
	newnode->data = value;
	newnode->left = nullptr;
	newnode->right = nullptr;
	return newnode;
}

// 1
void insertBST(Node*& root, int value)
{
	if (root == nullptr)
	{
		root = NEWNODE(value);
		return;
	}
	if (value <= root->data)
	{
		insertBST(root->left, value);
	}
	else
	{
		insertBST(root->right, value);
	}
}

// 6
void inOrder(Node* root)
{
	if (root == nullptr)
	{
		return;
	}
	else
	{
		inOrder(root->left);
		cout << root->data << " ";
		inOrder(root->right);
	}
}

void createTree(Node*& root)
{
	std::cout << "Do you want to create a tree? y (yes) or n (no) \n";
	char response;
	std::cin >> response;

	if (response == 'y' || response == 'Y')
	{
		int data;
		std::cout << " root: "; std::cin >> data;

		root = NEWNODE(data);

		std::cout << "Left for node " << data << std::endl;
		createTree(root->left);

		std::cout << "Right for node " << data << std::endl;
		createTree(root->right);
	}
}

// 12
int getcountNodes(Node* root)
{
	if (root == nullptr) return 0;
	return (getcountNodes(root->left) + getcountNodes(root->right) + 1);
}

int sum_of_nodes(Node* root)
{
	if (root == nullptr)
	{
		return 0;
	}
	else
	{
		return root->data + sum_of_nodes(root->left) + sum_of_nodes(root->right);
	}

}

double average(Node* root)
{
	double res = (double)sum_of_nodes(root) / (double)getcountNodes(root);
	return res;
}

bool isEqual(Node* root, int x)
{
	if (root == nullptr)
	{
		return false;
	}
	return root->data == x
		|| isEqual(root->left, x)
		|| isEqual(root->right, x);
}

bool check(Node* root)
{
	if (root == nullptr)
	{
		return false;
	}
	return isEqual(root->left, root->data) || isEqual(root->right, root->data)
		|| check(root->right) || check(root->left);
}

// 2
Node* findNode(Node* root, int x)
{
	if (root == nullptr)
	{
		return nullptr;
	}
	if (root->data == x)
	{
		return root;
	}
	if (x < root->data)
	{
		findNode(root->left, x);
	}
	else
	{
		findNode(root->right, x);
	}
}

// 3
void delTree(Node* root)
{
	if (root == nullptr)
	{
		return;
	}

	delTree(root->left);
	delTree(root->right);
	delete root;
}

// 5
void copy1(Node* source, Node*& res)
{
	if (source == nullptr)
	{
		return;
	}

	res = NEWNODE(source->data);

	copy1(source->left, res->left);
	copy1(source->right, res->right);
}

// 7
void levelTraversal(Node* root)
{
	if (root == nullptr)
	{
		return;
	}

	Node* curr;
	queue<Node*> level;
	level.push(root);

	while (!level.empty())
	{
		curr = level.front();
		cout << curr->data << " ";

		if (curr->left != nullptr)
		{
			level.push(curr->left);
		}
		if (curr->right != nullptr)
		{
			level.push(curr->right);
		}
		level.pop();
	}
}

// 8 (za min e root->left)
int maxInBST(Node* root)
{
	if (root == nullptr)
	{
		return 0;
	}
	if (root->right == nullptr)
	{
		return root->data;
	}
	return maxInBST(root->right);
}

//9
int findMax(Node* root)
{
	if (root == nullptr)
	{
		return 0;
	}

	int res = root->data;
	int lres = findMax(root->left);
	int rres = findMax(root->right);
	if (lres > res)
	{
		res = lres;
	}
	if (rres > res)
	{
		res = rres;
	}
	return res;
}

// 10
int height(Node* root)
{
	if (root == nullptr)
	{
		return -1;
	}

	int left = height(root->left);
	int right = height(root->right);
	if (left < right)
	{
		return right + 1;
	}
	else
	{
		return left + 1;
	}
}

// 11
int countLeaves(Node* root)
{
	if (root == nullptr)
	{
		return 0;
	}
	if (root->left == nullptr && root->right == nullptr)
	{
		return 1;
	}
	return countLeaves(root->left) + countLeaves(root->right);
}

// 13
void mirror(Node* root)
{
	if (root == nullptr)
	{
		return;
	}
	mirror(root->left);
	mirror(root->right);
	Node* temp = root->left;
	root->left = root->right;
	root->right = temp;
}

// 14
void doubletree(Node* root)
{
	if (root == nullptr)
	{
		return;
	}
	doubletree(root->left);
	doubletree(root->right);

	Node* addition = root->left;
	root->left = NEWNODE(root->data);
	root->left->left = addition;
}

// 15
bool sameTree(Node* root1, Node* root2)
{
	if (root1 == nullptr && root2 == nullptr)
	{
		return true;
	}
	if (root1 != nullptr && root2 != nullptr)
	{
		return (root1->data == root2->data) && sameTree(root1->left, root2->left) && sameTree(root1->right, root2->right);
	}
	return false;
}

int main()
{
	Node* root = nullptr;
	Node* root2 = nullptr;

	while (true)
	{
		int i;
		cin >> i;
		if (i == 0) break;
		insertBST(root, i);
	}
	inOrder(root);
	cout << endl << endl;

	/*while (true)
	{
		int i;
		cin >> i;
		if (i == 0) break;
		insertBST(root2, i);
	}
	inOrder(root2);
	cout << endl;*/

	doubletree(root);
	inOrder(root);
	cout << endl;

	//cout << sameTree(root, root2) << "yyyyyyyyy";
	//createTree(root);

	//levelTraversal(root);
	//copy1(root, root2);
	//inOrder(root2);

	//cout << average(root);
	//cout << check(root);

	//cout << height(root);
	//cout << findMax(root) << "*********";

	return 0;
}
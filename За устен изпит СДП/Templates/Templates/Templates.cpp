﻿#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED
/*1.Да се напише шаблон на клас Stack,
реализиращ абстрактния тип данни Стек.
За реализацията използвайте динамичен масив,
който може да се разширява*/
//FIRST IN LAST OUT- pyrwi wliza posledniq izliza(STACK)
//FIRST IN FIRST OUT - (QUEUE)
/*The functions associated with stack are:
empty() – Returns whether the stack is empty -prowerqwa dali e prazen stack
size() – Returns the size of the stack-izkarwa razmera na stack
top() – Returns a reference to the top most element of the stack-vrushta prepratka do posledniq element
push(g) – Adds the element ‘g’ at the top of the stack- dobawq element g nai otgore na steka
pop() – Deletes the top most element of the stack- trie posledniq ot stack
*/
template<typename T>
class Stack
{
    private:
    static const int INITIAL_CAPACITY = 10;//razmer na stak
    /*instant variables */ // - momentalni promenliwi
    T* arr;//masiva elementi na stek
    int capacity;//kapacitet
    int Count;//broqqch
    /*private method prototypes*/
    void deepCopy(const Stack<T>& other);
    void expandCapacity();
    void Del();
public:
    //golqmata chertorka
    Stack();//konstruktor po podrazbirane       
    ~Stack();//dekonstruktor
    Stack(const Stack<T>& other);//kopi konstruktor
    Stack<T>& operator=(const Stack<T>& other);//operator =
    //funkcii
    int Size() const;//razmer
    bool isEmpty() const;//bulewa promenliwa dali e prazen
    void Clear();//da izchistim
    void push(T value);//wkarwa elementi
    void pop();//izkarwa elementi
    T top() const;//vrushta prepratka do posledniq element
    



};

template<typename T>
Stack<T>::Stack()//razpiswame konstruktora po podrazbirane
{
    capacity = INITIAL_CAPACITY;//kapaciteta da bude rawen na stojnostta na initial_capacity , toest 10
    arr = new T[capacity];//masiwa da e rawen na now shablon  s razmer kapaciteta, toest 10
    Count = 0;//broqcha da pochwa ot nula
}

template<typename T>
Stack<T>::~Stack()//dekonstruktora osvobojdawa dinamichnata pamet
{
    Del();
}
template<typename T>
Stack<T>::Stack(const Stack<T>& other)//kopi konstruktora wikame deepcopy(other)
{
    deepCopy(other);
}

template<typename T>
Stack<T>& Stack<T>::operator=(const Stack<T>& other)//operator = ako e razlichno ot other(drugiq stack),triem i kopirame drugiq stack
{
    if (this != &other)
    {
        Del();
        deepCopy(other);
    }
    return *this;
}

template<typename T>
int Stack<T>::Size() const//razmera wrushta broqcha
{
    return Count;
}

template<typename T>
bool Stack<T>::isEmpty() const//dali e prazen,bulewa promenliwa,koqto wrushta broqcha da bude rawen na 0,ako e prazen stack-a
{
    return Count == 0;
}

template<typename T>
void Stack<T>::Clear()//chisti steka,toest broqcha da bude rawen na 0
{
    Count = 0;
}

template<typename T>
void Stack<T>::push(T value)//tuk pushwame elementi w steka
{
    if (Count == capacity) expandCapacity();//ako broqcha e rawen na razmernostta, wikame expandCapacity
    arr[Count++] = value;//masiw[dobawqme edin element] i e rawen na value
}

template<typename T>
void Stack<T>::pop()//iztrivame elementa koito se namira na wurha na stack-a
{
    if (isEmpty())//no ako steka e prazen
    {
        std::cout << "pop: Attempting to pop an empty stack" << std::endl;//opitwame se da triem prazen stack
        exit(1);//zatowa wikame exit(1)
    }
    Count--;//broqcha swalqme s po edno nadolu dokato ne se izprazni,toest go triem
}

template<typename T>
T Stack<T>::top() const//vrushtame prepratka do posledniq element
{
    if (isEmpty())//no ako steka e prazen
    {
        std::cout << "peek: Attempting to peek an empty stack" << std::endl;//opitwame se da pregledame prazen stack
        exit(1);
    }
    return arr[Count - 1];//i ot masiwa s elementi stack wadim edin element,toest pokazwame nai gorniq element
}



template<typename T>
void Stack<T>::deepCopy(const Stack<T>& other)//funkciqta za kopirane koqto q izpolzwame s kopi konstruktora
{
    capacity = other.capacity + INITIAL_CAPACITY;//razmernostta na segashniq stack da bude ravna na razmernostta na
                                                                               // drugiq + initial_capacity,toest 10
    arr = new T[capacity];//togawa masiwa se prisvoqwa na drug masiw shablon s elementi capacity
    for (int i = 0; i < other.Count; i++)//prawim edin broqch ot 0 do broq elementi na drugiq stack
    {
        arr[i] = other.arr[i];//i ot ediniq stack pulnim elementite na drugiq stack
    }
    Count = other.Count;//prisvoqwame broqcha na ediniq stack vurhu drugiq

}

template<typename T>
void Stack<T>::expandCapacity()
{
    T* oldarray = arr;//stariq masiv da bude rawen na segashniq
    capacity *= 2;//togawa capaciteta go uvelichawame po 2
    arr = new T[capacity];//togawa segashniq masiw go priswoqwame na nowiq shablon masiv
    for (int i = 0; i < Count; i++)//ot 0 do broq elementi
    {
        arr[i] = oldarray[i];//i ot stariq pylnim w segashniq masiw elementite
    }

    delete[] oldarray;//deletevame stariq element
}

template<typename T>
void Stack<T>::Del()//funkciqta delete trie dinamichniq masiw arr
{
    delete[] arr;
}



#endif // STACK_H_INCLUDED
//------------------------------------------------------------
/*2.Да се напиише шаблон на класс Queue,реализиращ абстрактния тип данни Опашка.
За реализацията използвайте динамичен масив,който може да се разширява.*/
#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED
#include <iostream>
template<typename T>
class Queue
{
public:
    Queue();
    ~Queue();
    int Size() const;
    bool isEmpty() const;
    void Clear();
    void push(T value);
    void pop();
    T front() const;
    //! Copy constructor
    Queue(const Queue<T>& other);
    //! Assignment operator
    Queue<T>& operator=(const Queue<T>& other);

private:
    /*private constants */
    static const int INITIAL_CAPACITY = 10;

    /*instant variables */
    T* arr;
    int capacity;
    int head;
    int tail;

    /*private method prototypes*/
    void deepCopy(const Queue<T>& other);
    void expandCapacity();
    void Del();

};

template<typename T>
Queue<T>::Queue()
{
    capacity = INITIAL_CAPACITY;
    arr = new T[capacity];
    head = 0;
    tail = 0;
}

template<typename T>
Queue<T>::~Queue()
{
    Del();
}

template<typename T>
int Queue<T>::Size() const
{
    return (tail - head + capacity) % capacity;
}

template<typename T>
bool Queue<T>::isEmpty() const
{
    return head == tail;
}

template<typename T>
void Queue<T>::Clear()
{
    head = 0;
    tail = 0;
}

template<typename T>
void Queue<T>::push(T value)
{
    if (Size() == capacity - 1) expandCapacity();
    arr[tail] = value;
    tail = (tail + 1) % capacity;
}

template<typename T>
void Queue<T>::pop()
{
    if (isEmpty())
    {
        std::cout << "pop: Attempting to pop an empty Queue" << std::endl;
        exit(1);
    }
    head = (head + 1) % capacity;;
}

template<typename T>
T Queue<T>::front() const
{
    if (isEmpty())
    {
        std::cout << "front: Attempting to get front of an empty Queue" << std::endl;
        exit(1);
    }
    return arr[head];
}

template<typename T>
Queue<T>::Queue(const Queue<T>& other)
{
    deepCopy(other);
}

template<typename T>
Queue<T>& Queue<T>::operator=(const Queue<T>& other)
{
    if (this != &other)
    {
        Del();
        deepCopy(other);
    }
    return *this;
}

template<typename T>
void Queue<T>::deepCopy(const Queue<T>& other)
{
    int count = other.Size();
    capacity = count + INITIAL_CAPACITY;
    arr = new T[capacity];

    for (int i = 0; i < count; i++)
    {
        arr[i] = other.arr[(other.head + i) % other.capacity];
    }

    head = 0;
    tail = count;
}

template<typename T>
void Queue<T>::expandCapacity()
{
    T* oldarray = arr;
    arr = new T[2 * capacity];

    int count = Size();
    for (int i = 0; i < count; i++)
    {
        arr[i] = oldarray[(head + i) % capacity];
    }

    head = 0;
    tail = count;
    capacity *= 2;

    delete[] oldarray;
}

template<typename T>
void Queue<T>::Del()
{
    delete[] arr;
}



#endif // QUEUE_H_INCLUDED
//-----------------------------------------------------------------------------
/*3.Да се напише шаблон на клас Vector,реализиращ абстрактния тип данни Вектор. 
За реализацията използвайте динамичен масив,който може да се разширява.*/
#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <iostream>

template <typename T>
class Vector
{
public:

    Vector();
    Vector(int n, T value = T());
    ~Vector();
    int size() const;
    bool isEmpty() const;
    void clear();
    T get(int index) const;
    void set(int index, T value);
    void insert(int index, T value);
    void remove(int index);
    void add(T value);
    T& operator[](int index);
    Vector(const Vector<T>& other);
    Vector<T>& operator=(const Vector<T>& other);

private:

    /* private constants */
    static const int INITIAL_CAPACITY = 10;

    /* instant variables */

    T* arr;
    int capacity;
    int count;

    /* private method prototypes */

    void DeepCopy(const Vector<T>& other);
    void ExpandCapacity();

};

template <typename T>
Vector<T>::Vector()
{
    capacity = INITIAL_CAPACITY;
    count = 0;
    arr = new T[capacity];
}

template <typename T>
Vector<T>::Vector(int n, T value)
{
    capacity = (n > INITIAL_CAPACITY) ? n : INITIAL_CAPACITY;
    arr = new T[capacity];
    count = n;
    for (int i = 0; i < n; i++)
    {
        arr[i] = value;
    }
}

template <typename T>
Vector<T>::~Vector()
{
    delete[] arr;
}

template <typename T>
int Vector<T>::size() const
{
    return count;
}

template <typename T>
bool Vector<T>::isEmpty() const
{
    return count == 0;
}

template <typename T>
void Vector<T>::clear()
{
    count = 0;
}

template <typename T>
T Vector<T>::get(int index) const
{
    if (index < 0 || index >= count)
    {
        std::cerr << "get: index out of range";
        exit(1);
    }
    return arr[index];
}

template <typename T>
void Vector<T>::set(int index, T value)
{
    if (index < 0 || index >= count)
    {
        std::cerr << "set: index out of range";
        exit(1);
    }
    arr[index] = value;
}

template <typename T>
void Vector<T>::insert(int index, T value)
{
    if (index < 0 || index>count)
    {
        std::cerr << "insert: index out of range";
        exit(1);
    }
    if (count == capacity) ExpandCapacity();


    for (int i = count; i > index; i--)
    {
        arr[i] = arr[i - 1];
    }
    arr[index] = value;
    count++;

}

template <typename T>
void Vector<T>::remove(int index)
{

    if (index < 0 || index >= count)
    {
        std::cerr << "remove: index out of range";
        exit(1);
    }
    for (int i = index; i < count - 1; i++)
    {
        arr[i] = arr[i + 1];
    }
    count--;
}

template <typename T>
void Vector<T>::add(T value)
{
    insert(count, value);
}

template <typename T>
T& Vector<T>::operator[](int index)
{
    if (index < 0 || index >= count)
    {
        std::cerr << "Vector index out of range";
        exit(1);
    }
    return arr[index];
}

template <typename T>
Vector<T>::Vector(const Vector<T>& other)
{
    DeepCopy(other);
}

template <typename T>
Vector<T>& Vector<T>::operator=(const Vector<T>& other)
{
    if (this != &other)
    {
        delete[] arr;
        DeepCopy(other);
    }
    return *this;
}

template <typename T>
void Vector<T>::DeepCopy(const Vector<T>& other)
{
    capacity = other.count + INITIAL_CAPACITY;
    count = other.count;
    arr = new T[capacity];
    for (int i = 0; i < count; i++)
    {
        arr[i] = other.arr[i];
    }

}

template <typename T>
void Vector<T>::ExpandCapacity()
{
    T* oldarray = arr;
    capacity *= 2;
    arr = new T[capacity];
    for (int i = 0; i < count; i++)
    {
        arr[i] = oldarray[i];
    }
    delete[] oldarray;
}



#endif // VECTOR_H_INCLUDED

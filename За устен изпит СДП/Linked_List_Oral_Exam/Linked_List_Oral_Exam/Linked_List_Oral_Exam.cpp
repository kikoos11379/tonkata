﻿#include <iostream>
#include <stack>

using namespace std;

//Задача 3
/*Напишете функция DeleteList(),която взема свързан списък
и го изтрива целия (освобождава динамичната памет, която е
зададена на него) и сетва указателя head на nullptr*/

struct Node
{
	int data;
	Node* next;
};
void Insert_begin(Node*& head, int x)//push-ва елемент в началото
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}
void DeleteList(Node*& head)//указател към възел,значи звезда,
//но този списък ще се изменя,head няма да сочи към първия възел и ще се изменя,
//затова слагаме &
{
	Node* current = head;
	Node* temp;
	while (current != nullptr)
	{
		temp = current;
		current = current->next;
		delete temp;
	}
	head = nullptr;
}
void PrintList(Node* head)
{
	cout << "List is: ";
	while (head != nullptr)
	{
		cout << head->data << ", ";
		head = head->next;
	}
	cout << endl;
}
int main()
{
	Node* head = nullptr;
	int x, n;
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		Insert_begin(head, i);
	}
	PrintList(head);
	DeleteList(head);
	PrintList(head);

	system("pause");
	return 0;
}

//Задача 4
/*Напишете функция CopyList(),която взема свързан списък и връща негово копие.*/
struct Node
{
	int data;
	Node* next;
};

void Insert(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}


Node* CopyList(Node* head)//когато обхождам един списък и не го променям,
//можем да ъпдейтнем head,защото е локална променлива и няма да промени оригиналния списък
{
	Node dummy;
	Node* tail = &dummy;
	dummy.next = nullptr;
	while (head != nullptr)
	{
		tail->next = new Node;//създавам нов възел
		tail = tail->next;//придвижвам се напред
		tail->data = head->data;//попълвам данните от head
		head = head->next;//продължавам напред
	}
	tail->next = nullptr;//последния възел да е nullptr
	return dummy.next;// и връщам dummny.next
}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int main()
{
	Node* head = nullptr;
	int x, n;
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Enter new data:  ";
		cin >> x;
		Insert(head, x);
		PrintList(head);
	}

	Node* newList = CopyList(head);
	PrintList(newList);
}

//Задача 6
//Напишете функция FrontBackSplit(),която взема свързан списък
//и го разделя на две половини (предна и задна). Ако броят на 
//възлите е нечетен, предната част има с един възел повече.

struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}

void FrontBackSplit(Node* head, Node*& Front, Node*& Back)
{

	if (head == nullptr)//в случая на празен списък
	{
		Front = head;
		Back = nullptr;
	}
	else
	{
		Node* slowptr = head;//бавен ptr
		Node* fastptr = head->next;//бърз ptr
		while (fastptr != nullptr)
		{
			fastptr = fastptr->next;//бързия ptr го придвижвам напред
			if (fastptr != nullptr)
			{
				fastptr = fastptr->next;//бързия ptr го придвижвам напред
				slowptr = slowptr->next;//бавния ptr го придвижвам напред
			}
		}//така правим бързия ptr да се придвижва два пъти по-бързо от бавния ptr
		Back = slowptr->next;
		slowptr->next = nullptr;//да завърша front
		Front = head;//го насочваме,където сочи head
	}


}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int main()
{

	Node* head = nullptr;
	Node* Front = nullptr;
	Node* Back = nullptr;
	for (int i = 9; i >= 1; i--)
	{
		Insert_begin(head, i);
	}
	PrintList(head);
	FrontBackSplit(head, Front, Back);
	PrintList(Front);
	PrintList(Back);
}

//Задача 7
//Напишете функция RemoveDuplicates(),която взема сортиран във възходящ ред 
//свързан списък и изтрива дублиращите се възли

struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}

void DeleteDuplicates(Node* head)//другия вариант
{
	if (head == nullptr) return;
	Node* current = head;

	while (current->next != nullptr)
	{
		if (current->data == current->next->data)
		{
			Node* nextNext = current->next->next;
			delete current->next;
			current->next = nextNext;
		}
		else
		{
			current = current->next;
		}
	}
}

void DeleteDuplicates2(Node* head)//първия и по-добър вариант
{

	Node* current = head;//който го насочвам към първия указател
	Node* temp;//ще върви напред,докато стигне възел с различни данни
	Node* todelete;//ще трябва да трия този указател
	while (current != nullptr)//докато е различно от nullptr
	{
		int value = current->data;//запомням си стойността
		temp = current->next;//и го инициализирам до следващия възел
		while (temp != nullptr && temp->data == value)//докато следващия възел,който съществува 
		{
			todelete = temp;//запоммям
			temp = temp->next;//придвижвам напред
			delete todelete;//трия
		}
		current->next = temp;//свързвам към това,което сочи temp
		current = current->next;//current го местим

	}
}


void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int main()
{
	Node* head = nullptr;
	for (int i = 9; i >= 1; i--)
	{
		int x;
		cout << "Push an element:";
		cin >> x;
		Insert_begin(head, x);
	}
	PrintList(head);
	DeleteDuplicates2(head);
	PrintList(head);


}
//Задача 8
/*Напишете функция MoveNode(), която взема два свързани списъка a и b,
премахва първия възел от втория списък и го премества в началото на първия списък.
Ако втория списък е празен,не се случва нищо(ТОВА Е А ПОДТОЧКА)*/
/*Използвайки MoveNode() напишете функция AlternatingSplit(),
която взема свързан списък, от който образува два по-малки списъка получени по следния начин:
Всички елементи на нечетна позиция влизат в първия списък, а тези на четна, във втория, при това са
в обратния ред от този в оригиналния списък. Например от списъка {1,2,3,4,5,6} се получават списъците {5,3,1} и {6,4,2}
(ТОВА Е Б ПОДТОЧКА)*/
struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}


void MoveNode(Node*& dest, Node*& source)//А подточка на задачата
{
	if (source != nullptr)//втори Node дали съществува проверяваме,тоест втория списък дали не е празен
	{
		Node* temp = source;
		source = source->next;//прескачаме първия възел от втория списък и отиваме директно на следващия възел
		temp->next = dest;//първия възел от втория списък го прехвърляме на първия възел на първия списък
		dest = temp;//указателя от първия списък отива към първия възел на втория списък
	}
}

void AlternatingSplit(Node*& source, Node*& first, Node*& second)//Б подточка на задачата
{
	first = nullptr;
	second = nullptr;
	while (source != nullptr)//това е главния възел,който ще го унищожим
	{
		MoveNode(first, source);//движим от source към first
		if (source != nullptr)//ако има нещо,което е останало
			MoveNode(second, source);//продължаваме да движим от source до second
	}
}

void AlternatingSplit2(Node* source, Node*& first, Node*& second)//Това е ЗАДАЧА 6,която се различава по това,че изкарва двата списъка в нормален ред,както е в оригиналния,а не в обратен ред
{
	Node dummyfirst;
	Node* tailf = &dummyfirst;
	dummyfirst.next = nullptr;

	Node dummysecond;
	Node* tails = &dummysecond;;
	dummysecond.next = nullptr;


	while (source != nullptr)
	{
		tailf->next = source;
		tailf = source;
		source = source->next;
		if (source != nullptr)
		{
			tails->next = source;
			tails = source;
			source = source->next;
		}
	}

	tailf->next = nullptr;
	tails->next = nullptr;
	first = dummyfirst.next;
	second = dummysecond.next;
}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int main()
{
	Node* head = nullptr;
	for (int i = 10; i >= 1; i--)
	{
		Insert_begin(head, i);
	}
	Node* first = nullptr;
	Node* second = nullptr;
	PrintList(head);
	AlternatingSplit(head, first, second);
	PrintList(first);
	PrintList(second);
	PrintList(head);
	//---------------------------------------------------
	Node* head1 = nullptr;//това е задача 6
	for (int i = 11; i >= 1; i--)
	{
		Insert_begin(head1, i);
	}
	Node* first1 = nullptr;
	Node* second1 = nullptr;
	PrintList(head1);
	AlternatingSplit2(head1, first1, second1);
	PrintList(first1);
	PrintList(second1);
	PrintList(head1);
	//---------------------------------------------------
}
//Задача 9
/*Напишете функция AlternatingSplit2(),която се отличава
от AlternatingSplit() само по това,че редът от по-малките
списъци да е същия като на оригиналния. Например от списъка
{1,2,3,4,5,6} се получават списъците {1,3,5},{2,4,6}*/
struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}


void MoveNode(Node*& dest, Node*& source)
{
	if (source != nullptr)
	{
		Node* temp = source;
		source = source->next;
		temp->next = dest;
		dest = temp;
	}
}

void AlternatingSplit2(Node* source, Node*& first, Node*& second)
{
	Node dummyfirst;
	Node* tailf = &dummyfirst;
	dummyfirst.next = nullptr;

	Node dummysecond;
	Node* tails = &dummysecond;;
	dummysecond.next = nullptr;


	while (source != nullptr)
	{
		tailf->next = source;
		tailf = source;
		source = source->next;
		if (source != nullptr)
		{
			tails->next = source;
			tails = source;
			source = source->next;
		}
	}

	tailf->next = nullptr;
	tails->next = nullptr;
	first = dummyfirst.next;
	second = dummysecond.next;
}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int main()
{
	Node* head1 = nullptr;
	for (int i = 11; i >= 1; i--)
	{
		Insert_begin(head1, i);
	}
	Node* first1 = nullptr;
	Node* second1 = nullptr;
	PrintList(head1);
	AlternatingSplit2(head1, first1, second1);
	PrintList(first1);
	PrintList(second1);
	PrintList(head1);

}
//Задача 11
//Напишете функция SortedMerge(), която взема два сортирани във
//възходящ ред свързани списъка и ги слива в нов сортиран списък,
//който се връща от функцията.
struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

Node* SortedMerge(Node* a, Node* b)//A подточка
{
	Node dummy;
	Node* tail = &dummy;
	dummy.next = nullptr;
	while (true)
	{
		if (a == nullptr)
		{
			tail->next = b;
			break;
		}
		else if (b == nullptr)
		{
			tail->next = a;
			break;
		}
		if (a->data < b->data)
		{
			tail->next = a;
			tail = a;
			a = a->next;
		}
		else
		{
			tail->next = b;
			tail = b;
			b = b->next;
		}
	}

	return dummy.next;
}

void FrontBackSplit(Node* head, Node*& Front, Node*& Back)//Б подточка
{

	if (head == nullptr) //length<2 cases
	{
		Front = head;
		Back = nullptr;
	}
	else
	{
		Node* slowptr = head;
		Node* fastptr = head->next;
		while (fastptr != nullptr)
		{
			fastptr = fastptr->next;
			if (fastptr != nullptr)
			{
				fastptr = fastptr->next;
				slowptr = slowptr->next;
			}
		}
		Back = slowptr->next;
		slowptr->next = nullptr;
		Front = head;
	}


}

void MergeSort(Node*& head)//B подточка
//Използвайки FrontBackSplit() и SortedМеrge(),
//напишете класическа рекурсивна функция MergeSort(),
//която сортира свързан списък във възходящ ред.
{
	if (head == nullptr || head->next == nullptr) return;
	Node* Front = nullptr;
	Node* Back = nullptr;
	FrontBackSplit(head, Front, Back);
	MergeSort(Front);
	MergeSort(Back);
	head = SortedMerge(Front, Back);

}

int main()
{
	Node* a = nullptr;
	for (int i = 2; i >= -3; i--)
	{
		Insert_begin(a, i);
	}
	Node* b = nullptr;
	for (int i = -1; i >= -5; i--)
	{
		Insert_begin(b, i);
	}
	PrintList(a);
	PrintList(b);
	Node* head1 = SortedMerge(a, b);
	PrintList(head1);

	Node* head = nullptr;
	for (int i = 1; i <= 10; i++)
	{
		int x;
		cout << "Push new element: ";
		cin >> x;
		Insert_begin(head, x);
	}
	PrintList(head);
	MergeSort(head);
	PrintList(head);

}
//Задача 15
/*Напишете функция Reverse(), която обръща свързан списък
като пренарежда всички указатели next и указателя head.
Функцията да прави само едно преминаване през масива.
При преминаването през списъка правете "Push-ване" ( без създаване на нов възел).
Както знаете при push редът се обръща.*/
struct Node
{
	int data;
	Node* link;
};

void Insert(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->link = head;
	head = temp;
}



void recursiveReverse(Node*& head)
{
	if (head == nullptr || head->link == nullptr) return;

	Node* next = head->link;
	recursiveReverse(next);
	head->link->link = head;
	head->link = nullptr;
	head = next;

}

void iterativeReverse(Node*& head)
{
	Node* result = nullptr;
	Node* current = head;
	Node* next;
	while (current != nullptr)
	{
		next = current->link;
		current->link = result;
		result = current;
		current = next;
	}
	head = result;
}



void stackReverse(Node*& head)
{
	if (head == nullptr)
	{
		return;
	}
	Node* temp = head;
	stack<Node*> st;
	while (temp->link != nullptr)
	{
		st.push(temp);
		temp = temp->link;
	}
	head = temp;
	while (!st.empty())
	{
		temp->link = st.top();
		st.pop();
		temp = temp->link;
	}
	temp->link = nullptr;
}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->link;
	}
	cout << endl;
}

int main()
{
	Node* head = nullptr;
	int x, n;
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Enter new data:  ";
		cin >> x;
		Insert(head, x);
		PrintList(head);
	}
	recursiveReverse(head);
	PrintList(head);
	iterativeReverse(head);
	PrintList(head);
	stackReverse(head);
	PrintList(head);
}
//============================================================
//============================================================
//============================================================
//Задача 1
/*Да се напишат функции,които добавят нов елемент в началото и нов елемент в края на списъка.
За да проверите дали фунцкиите работят правилно,напишете и функция за отпечатване на списъка.
Функцията,която добавя нов елемент в началото на списъка често се нарича Push(),макар в решенията,
които сме дали, да я наричаме Insert_begin() */

struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}

void Insert_end(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = nullptr;
	if (head == nullptr)
	{
		head = temp;
	}
	else
	{
		Node* temp1 = head;
		while (temp1->next != nullptr)
		{
			temp1 = temp1->next;
		}
		temp1->next = temp;
	}


}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int main()
{
	Node* head = nullptr;
	int x, n;
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Enter new data:  ";
		cin >> x;
		if (i % 2 == 0)
		{
			Insert_begin(head, x);
		}
		else
		{
			Insert_end(head, x);
		}
		PrintList(head);
	}

}
//Задача 2
/*Да се напише функция,която връща дължината на даден списък*/

struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}



void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int length(Node* head)
{
	int l = 0;
	while (head != nullptr)
	{
		l++;
		head = head->next;
	}
	return l;
}

int main()
{
	Node* head = nullptr;
	int x, n;
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Enter new data:  ";
		cin >> x;
		Insert_begin(head, x);

	}
	PrintList(head);
	cout << "list length is: " << length(head);

}
//Напишете функция ПОП()(обратна на пуш),която изтрива първия възел(pop - маха,a push - вкарва)
//в свързания списък и връща стойността на данните му. Напишете и функция DeleteLast(),
//която изтрива последния възел в свързания списък и връща стойността на данните му.

struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = head;
	head = temp;
}



void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)
	{
		cout << head->data << "  ";
		head = head->next;
	}
	cout << endl;
}

int  Pop(Node*& head)
{
	if (head == nullptr)
	{
		cerr << "Error. Trying to pop an empty stack.";
		exit(1);
	}
	int result = head->data;
	Node* temp = head;
	head = head->next;
	delete temp;
	return result;
}


int  DeleteLast(Node*& head)
{
	if (head == nullptr) //!empty list
	{
		cerr << "Error. Trying to delete last node in an empty stack.";
		exit(1);
	}
	int result;
	if (head->next == nullptr) //!one node list
	{
		result = head->data;
		delete head;
		head = nullptr;
		return result;
	}
	Node* current = head;
	while (current->next->next != nullptr)
	{
		current = current->next;
	}
	result = current->next->data;
	delete current->next;
	current->next = nullptr;
	return result;

}


int main()
{
	Node* head = nullptr;
	int x, n;
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Enter new data:  ";
		cin >> x;
		Insert_begin(head, x);

	}
	cout << "Popping: " << Pop(head) << " ";
	PrintList(head);
	cout << "deleting last: " << DeleteLast(head) << " ";
	PrintList(head);
	cout << "Popping: " << Pop(head) << " ";
	PrintList(head);
	cout << "deleting last: " << DeleteLast(head) << " ";
	PrintList(head);

}
//Задача 4 и 5та
/*Да се напише функция,която за дадено n>-1 добавя нов елемент на n-ta позиция(n>=1),
ако такава позиция съществува или в края на списъка,ако такава позиция не съществува*/
//Задача 5
//Да се напише функция,която за дадено n>-1 изтрива елемента на n-ta позиция,ако такава 
//позиция съществува или извежда съобщение,че такава позиция не съществува

#include <iostream>
using namespace std;

struct Node
{
	int data;
	Node* next;
};

void Insert(Node*& head, int x, int n)
{
	Node* temp = new Node;
	temp->data = x;
	if (head == nullptr || n == 1)
	{
		temp->next = head;
		head = temp;
	}
	else
	{
		Node* p = head;
		int i = 1;
		while (p->next != nullptr && i <= n - 2)
		{
			p = p->next;
			i++;
		}
		temp->next = p->next;
		p->next = temp;
	}
}

void Delete(Node*& head, int n)
{
	Node* p = head;
	if (head == nullptr)
	{
		cout << "The list is empty. There is no such element" << endl;
		return;
	}
	if (n == 1)
	{
		head = p->next;
		delete p;
	}
	else
	{
		int i = 1;
		while (i < n - 1 && p->next != nullptr)
		{
			p = p->next;
			i++;
		}

		if (i == n - 1 && p->next != nullptr)
		{
			Node* temp = p->next;
			p->next = temp->next;
			delete temp;
		}
		else
		{
			cout << "There is no such element " << endl;
		}
	}
}

void PrintList(Node* head)
{
	cout << "List is : ";
	while (head != nullptr)//ако възела не е празен,тоест указателя не е равен на нуллптр
	{
		cout << head->data << "  ";//принтираме текущия
		head = head->next;//и се движим напред
	}
	cout << endl;
}

int main()
{
	Node* head = nullptr;
	Delete(head, 1);
	int x;
	cout << "Enter new data:  ";
	cin >> x;
	Insert(head, x, 2);
	PrintList(head);
	cout << "Enter new data:  ";
	cin >> x;
	Insert(head, x, 1);
	PrintList(head);
	cout << "Enter new data:  ";
	cin >> x;
	Insert(head, x, 2);
	PrintList(head);
	cout << "Enter new data:  ";
	cin >> x;
	Insert(head, x, 2);
	PrintList(head);
	cout << "Enter new data:  ";
	cin >> x;
	Insert(head, x, 3);
	PrintList(head);

	Delete(head, 1);
	PrintList(head);
	Delete(head, 4);
	PrintList(head);
	Delete(head, 3);
	PrintList(head);
	Delete(head, 3);
	PrintList(head);
}
//Задача 6
/*Да се напишат рекурсивни функции за отпечатване на свързан списък в прав или обратен ред*/

#include <iostream>
using namespace std;

struct Node
{
	int data;
	Node* next;
};

void Insert_begin(Node*& head, int x)
{
	Node* temp = new Node;//създаваме нов възел
	temp->data = x;//текущия елемент да е равен на х
	temp->next = head;// следващия елемент да сочи към хеад
	head = temp;//хеад да бъде равен на  новия възел
}

void PrintListRecforward(Node* head)
{
	if (head == nullptr)
	{
		return;
	}
	cout << head->data << "  ";//първо текущия елемент да принтира
	PrintListRecforward(head->next);//после следващия
}
void PrintListRecbackward(Node* head)//да го принтираме назад
{

	if (head == nullptr)//ако хеад(указателя) е нуллптр
	{
		return;//ни връща
	}
	PrintListRecbackward(head->next);//иначе принтираме следващия
	cout << head->data << "  ";//след това прочетем текущия
}


int main()
{
	Node* head = nullptr;//създаваме един възел,който да започва с указател,който да е нуллптр
	int x, n;//н-това са броя елементи на възела, а х- е всяко от числата,които въвеждаме
	cout << "Enter the number of elements you want to insert:  ";
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cout << "Enter new data:  ";
		cin >> x;
		Insert_begin(head, x);
		cout << "Printed forward: ";
		PrintListRecforward(head);
		cout << endl;
		cout << "Printed backward: ";
		PrintListRecbackward(head);
		cout << endl;
	}

}

#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED
/**/
template<typename T>
class Stack
{
public:
    Stack();
    ~Stack();
    int Size() const;
    bool isEmpty() const;
    void Clear();
    void push(T value);
    void pop();
    T top() const;
    //! Copy constructor
    Stack(const Stack<T>& other);
    //! assignment operator
    Stack<T>& operator=(const Stack<T>& other);

private:

    static const int INITIAL_CAPACITY = 10;
    /*instant variables */
    T* arr;
    int capacity;
    int Count;
    /*private method prototypes*/
    void deepCopy(const Stack<T>& other);
    void expandCapacity();
    void Del();

};

template<typename T>
Stack<T>::Stack()
{
    capacity=INITIAL_CAPACITY;
    arr= new T[capacity];
    Count=0;
}

template<typename T>
Stack<T>::~Stack()
{
   Del();
}

template<typename T>
int Stack<T>::Size() const
{
    return Count;
}

template<typename T>
bool Stack<T>::isEmpty() const
{
    return Count==0;
}

template<typename T>
void Stack<T>::Clear()
{
    Count=0;
}

template<typename T>
void Stack<T>::push(T value)
{
    if (Count==capacity) expandCapacity();
    arr[Count++]=value;
}

template<typename T>
void Stack<T>::pop()
{
    if (isEmpty())
    {
        std::cout << "pop: Attempting to pop an empty stack" << std::endl;
        exit(1);
    }
    Count--;
}

template<typename T>
T Stack<T>::top() const
{
    if (isEmpty())
    {
        std::cout << "peek: Attempting to peek an empty stack" << std::endl;
        exit(1);
    }
    return arr[Count-1];
}

template<typename T>
Stack<T>::Stack(const Stack<T>& other)
{
    deepCopy(other);
}

template<typename T>
Stack<T>&  Stack<T>::operator=(const Stack<T>& other)
{
    if(this!=&other)
    {
        Del();
        deepCopy(other);
    }
    return *this;
}

template<typename T>
void Stack<T>::deepCopy(const Stack<T>& other)
{
    capacity=other.capacity + INITIAL_CAPACITY;
    arr = new T[capacity];
    for (int i=0; i<other.Count; i++)
    {
        arr[i]=other.arr[i];
    }
    Count=other.Count;

}

template<typename T>
void Stack<T>::expandCapacity()
{
    T* oldarray=arr;
    capacity*=2;
    arr = new T[capacity];
    for (int i=0; i< Count; i++)
    {
        arr[i]=oldarray[i];
    }

    delete [] oldarray;
}

template<typename T>
void Stack<T>::Del()
{
    delete [] arr;
}



#endif // STACK_H_INCLUDED

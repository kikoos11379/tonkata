#ifndef QUEUE_H_INCLUDED
#define QUEUE_H_INCLUDED
#include <iostream>
template<typename T>
class Queue
{
public:
    Queue();
    ~Queue();
    int Size() const;
    bool isEmpty() const;
    void Clear();
    void push(T value);
    void pop();
    T front() const;
    //! Copy constructor
    Queue(const Queue<T>& other);
    //! Assignment operator
    Queue<T>& operator=(const Queue<T>& other);

private:
    /*private constants */
    static const int INITIAL_CAPACITY = 10;

    /*instant variables */
    T* arr;
    int capacity;
    int head;
    int tail;

    /*private method prototypes*/
    void deepCopy(const Queue<T>& other);
    void expandCapacity();
    void Del();

};

template<typename T>
Queue<T>::Queue()
{
    capacity=INITIAL_CAPACITY;
    arr= new T[capacity];
    head=0;
    tail=0;
}

template<typename T>
Queue<T>::~Queue()
{
   Del();
}

template<typename T>
int Queue<T>::Size() const
{
    return (tail-head+capacity)%capacity;
}

template<typename T>
bool Queue<T>::isEmpty() const
{
    return head==tail;
}

template<typename T>
void Queue<T>::Clear()
{
    head=0;
    tail=0;
}

template<typename T>
void Queue<T>::push(T value)
{
    if (Size()==capacity-1) expandCapacity();
    arr[tail]=value;
    tail=(tail+1)%capacity;
}

template<typename T>
void Queue<T>::pop()
{
    if (isEmpty())
    {
        std::cout << "pop: Attempting to pop an empty Queue" << std::endl;
        exit(1);
    }
    head=(head+1)%capacity;;
}

template<typename T>
T Queue<T>::front() const
{
    if (isEmpty())
    {
        std::cout << "front: Attempting to get front of an empty Queue" << std::endl;
        exit(1);
    }
    return arr[head];
}

template<typename T>
Queue<T>::Queue(const Queue<T>& other)
{
    deepCopy(other);
}

template<typename T>
Queue<T>&  Queue<T>::operator=(const Queue<T>& other)
{
    if(this!=&other)
    {
        Del();
        deepCopy(other);
    }
    return *this;
}

template<typename T>
void Queue<T>::deepCopy(const Queue<T>& other)
{
    int count=other.Size();
    capacity=count + INITIAL_CAPACITY;
    arr = new T[capacity];

    for (int i=0; i<count; i++)
    {
        arr[i]=other.arr[(other.head+i)%other.capacity];
    }

    head=0;
    tail=count;
}

template<typename T>
void Queue<T>::expandCapacity()
{
    T* oldarray=arr;
    arr = new T[2*capacity];

    int count=Size();
    for (int i=0; i< count; i++)
    {
        arr[i]=oldarray[(head+i)%capacity];
    }

    head=0;
    tail=count;
    capacity*=2;

    delete [] oldarray;
}

template<typename T>
void Queue<T>::Del()
{
    delete [] arr;
}



#endif // QUEUE_H_INCLUDED

#ifndef VECTOR_H_INCLUDED
#define VECTOR_H_INCLUDED

#include <iostream>

template <typename T>
class Vector
{
public:

    Vector();
    Vector(int n, T value = T());
    ~Vector();
    int size() const;
    bool isEmpty() const;
    void clear();
    T get(int index) const;
    void set(int index, T value);
    void insert(int index, T value);
    void remove(int index);
    void add(T value);
    T& operator[](int index);
    Vector(const Vector<T>& other);
    Vector<T>& operator=(const Vector<T>& other);

private:

    /* private constants */
    static const int INITIAL_CAPACITY=10;

    /* instant variables */

    T* arr;
    int capacity;
    int count;

    /* private method prototypes */

    void DeepCopy(const Vector<T>& other);
    void ExpandCapacity();

};

    template <typename T>
    Vector<T>::Vector()
    {
        capacity=INITIAL_CAPACITY;
        count=0;
        arr = new T[capacity];
    }

    template <typename T>
    Vector<T>::Vector(int n, T value)
    {
        capacity = (n>INITIAL_CAPACITY) ? n : INITIAL_CAPACITY;
        arr = new T[capacity];
        count=n;
        for (int i=0; i<n; i++)
        {
            arr[i]=value;
        }
    }

    template <typename T>
    Vector<T>::~Vector()
    {
        delete [] arr;
    }

    template <typename T>
    int Vector<T>::size() const
    {
        return count;
    }

    template <typename T>
    bool Vector<T>::isEmpty() const
    {
        return count==0;
    }

    template <typename T>
    void Vector<T>::clear()
    {
        count = 0;
    }

    template <typename T>
    T Vector<T>::get(int index) const
    {
        if(index < 0 || index>=count)
        {
            std::cerr << "get: index out of range";
            exit(1);
        }
        return arr[index];
    }

    template <typename T>
    void Vector<T>::set(int index, T value)
    {
        if(index < 0 || index>=count)
        {
            std::cerr << "set: index out of range";
            exit(1);
        }
        arr[index]=value;
    }

    template <typename T>
    void Vector<T>::insert(int index, T value)
    {
        if(index < 0 || index>count)
        {
            std::cerr << "insert: index out of range";
            exit(1);
        }
        if(count==capacity) ExpandCapacity();


        for (int i=count; i>index; i--)
        {
            arr[i]=arr[i-1];
        }
        arr[index]=value;
        count++;

    }

    template <typename T>
    void Vector<T>::remove(int index)
    {

        if(index < 0 || index>=count)
        {
            std::cerr << "remove: index out of range";
            exit(1);
        }
        for (int i=index; i<count-1; i++)
        {
            arr[i]=arr[i+1];
        }
        count--;
    }

    template <typename T>
    void Vector<T>::add(T value)
    {
        insert(count,value);
    }

    template <typename T>
    T& Vector<T>::operator[](int index)
    {
        if(index < 0 || index>=count)
        {
            std::cerr << "Vector index out of range";
            exit(1);
        }
        return arr[index];
    }

    template <typename T>
    Vector<T>::Vector(const Vector<T>& other)
    {
        DeepCopy(other);
    }

    template <typename T>
    Vector<T>& Vector<T>::operator=(const Vector<T>& other)
    {
        if(this!=&other)
        {
            delete [] arr;
            DeepCopy(other);
        }
        return *this;
    }

    template <typename T>
    void Vector<T>::DeepCopy(const Vector<T>& other)
    {
        capacity=other.count + INITIAL_CAPACITY;
        count=other.count;
        arr=new T[capacity];
        for(int i=0; i<count; i++)
        {
            arr[i]=other.arr[i];
        }

    }

    template <typename T>
    void Vector<T>::ExpandCapacity()
    {
        T* oldarray=arr;
        capacity*=2;
        arr=new T[capacity];
        for (int i=0; i<count; i++)
        {
            arr[i]=oldarray[i];
        }
        delete [] oldarray;
    }



#endif // VECTOR_H_INCLUDED
